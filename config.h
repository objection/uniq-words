#define DEFAULT_WORD_LIST "/usr/share/dict/words"
// These numbers are pretty arbitrary. Changing them will probably make
// no perceptible difference.
#define INITIAL_SCANNED_FILE_ALLOC 5000
#define INITIAL_LIST_ALLOC 10000
// You might want to free the buf before the program ends. Maybe you're
// memory-constrained.
#define FREE_BUF 0
