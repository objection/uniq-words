#define _GNU_SOURCE
#include "config.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <darr.h>
#include <ctype.h>
#include <useful.h>
#include <search.h>
#include <err.h>
#include <stdbool.h>
#include <argp.h>

#define each(_elem, _n, _arr) for (auto _elem = _arr; _elem < _arr + _n; (_elem)++)
enum args_flags {
	ARGS_FLAG_ALWAYS_DEFAULT_LIST = 1 << 0,
};
typedef enum args_flags args_flags;

$make_arr (char *, str_arr);
typedef struct str_arr str_arr;

typedef struct args args;
struct args {
	str_arr word_lists;
	str_arr scanned_files;
	enum args_flags flags;
};

typedef struct word_token word_token;
struct word_token {
	char *s, *e;
	bool is_all_digit; // For skipping words that are all digits.
						// I won't bother with an option for including
						// word_token that are all digits.
};

typedef struct word word;
struct word {
	const char *s;
	int line;
};

$make_arr (struct word, word_arr);
typedef struct word_arr word_arr;

static void get_word (word_token *word_token, int *line) {
	word_token->is_all_digit = 1;
	word_token->s = word_token->e;

	// Ignore punctuation and spaces.
	while (*word_token->s
			&& !isalnum (*word_token->s)) {
		if (*word_token->s == '\n')
			*line += 1;
		word_token->s++;
	}
	word_token->e = word_token->s + 1;

	// Get the word_token, stopping at punctuation.
	while (*word_token->e && !isspace (*word_token->e)
			&& !ispunct (*word_token->e)) {
		if (!isdigit (*word_token->e))
			word_token->is_all_digit = 0;
		word_token->e++;
	}
}

static int get_words (word_arr *pr, char *file) {
	int r = 0;
	int fd = open (file, O_RDONLY);
	if (fd == -1)
		err (1, "Couldn't open %s", file);

	struct stat sb = {};
	fstat (fd, &sb) != -1 ?: err (1, "fstat failed");

	char *buf = mmap (0, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (buf == MAP_FAILED)
		err (1, "Couldn't mmap %s", file);

	if (close (fd) == -1)
		err (1, "Couldn't %s fd", file);

	char *e = 0;
	int line = 1;

	word_token word_token = {.s = buf, .e = buf};
	while (get_word (&word_token, &line), *word_token.s != '\0') {
		if (word_token.is_all_digit) continue;

		// Chop off "'s"
		if (*(word_token.e - 1) == 's' && *(word_token.e - 2) == '\'')
			e = word_token.e - 2;
		else
			e = word_token.e;
		char *word_chars = 0;
		asprintf (&word_chars, "%.*s", (int) (e - word_token.s),
				word_token.s);
		arr_add (pr, ((word) {.s = word_chars, .line = line}));
	}
	!munmap (buf, sb.st_size) ?: err (1, "Couldn't unmap %s", file);
	return r;
}

static struct word_arr get_words_from_files (char **files,
		size_t n_files) {
	word_arr r  = {};
	each (file, n_files, files) {
		if (get_words (&r, *file))
			err (1, "failed");
	}
	return r;
}

int comparison (const void *a, const void *b) {
	return strcasecmp (((word *) a)->s, ((word *) b)->s);
}

static void print (word_arr *file_words, word_arr *word_list_words) {

	// For each word in the file
	each (file_word, file_words->n, file_words->d) {

		// If the word isn't found in the word list
		if (!bsearch (file_word, word_list_words->d, word_list_words->n,
					sizeof *word_list_words->d, comparison))

			printf ("%s\n", file_word->s);
	}
}


static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	args *args = state->input;
	switch (key) {
		case ARGP_KEY_INIT:
			*args = (struct args) {
				.word_lists = (str_arr) {0},
					.scanned_files = (str_arr) {0},
			};
			break;
		case ARGP_KEY_ARG:
			struct stat sb = {};
			int rt = stat (arg, &sb);
			if (rt)
				err (1, "stat failed");
			if ((sb.st_mode & S_IFMT) == S_IFDIR)
				break;
			arr_push (&args->scanned_files, arg);
			break;
		case ARGP_KEY_END:
			if (!(args->word_lists.n)
					|| (args->flags & ARGS_FLAG_ALWAYS_DEFAULT_LIST))
				arr_push (&args->word_lists, DEFAULT_WORD_LIST);
			if (!args->scanned_files.n)
				argp_usage (state);
			break;
		case 'd':
			args->flags |= ARGS_FLAG_ALWAYS_DEFAULT_LIST;
			break;
		case 'w':
			arr_push (&args->word_lists, arg);
			break;
		default:
			break;
	}
	return 0;
}

int main (int argc, char **argv) {

	args args;

	static struct argp_option options[] = {
		{"word-list", 'w', "FILE", 0, "\
Word list to use. Use -w again for multiple word_token lists"},
		{"default", 'd', 0, 0, "\
Include default word list even when adding extra"},
		{0},
	};

	static struct argp argp = {options, parse_opt, "[FILES ...]", "\
Find \"unique\" words in documents, AKA words that are not in --word-list.\
\v\
Default word list is " DEFAULT_WORD_LIST "."};
	argp_parse (&argp, argc, argv, 0, 0, &args);

	if (argc < 2) {
		fprintf (stderr, "usage: %s file <word list>\n", argv[0]);
		exit (1);
	}

	word_arr list = get_words_from_files (args.word_lists.d,
			args.word_lists.n);

	word_arr words = get_words_from_files (args.scanned_files.d,
			args.scanned_files.n);

	qsort (words.d, words.n, sizeof *words.d,
			comparison);
	qsort (list.d, list.n, sizeof *list.d, comparison);

#if FREE_BUF
	free (scanned_file.buf);
	free (list.buf);
#endif
	print (&words, &list);
	exit (0);
}
