# uniq-words

This finds "unique" words, ie words that are not in a word list.

The point of this is to look for, eg, names you've made up in your
story. Your story is this:

> Simon went to the shops. The end.

This program will print "Simon", assuming "Simon" isn't in your word
list. 

It takes two arguments, the file you're scanning, and the path of a
word list. The word list is optional. If you don't provide it, the
program will use /usr/share/dict/words. There doesn't seem to be an
equivalent word list in Windows.

## What's a word?

A word is any number of alphanumeric characters followed by whitespace
or the end of the file. Punctuation not in the middle of words is
ignored. "Words" that are just numbers are ignored.

This program obviously neglects non-Latin scripts. Well, it may well
work to some degree, but not because I've taken the time to make sure
it does.

## Install

Install Meson, the build system.

```
git clone --recursive 'https://gitlab.com/objection/uniq-words'
cd uniq-words
meson build
ninja -Cbuild
```

If you want to install it system-wide, do `ninja -Cbuild` with
`sudo ninja -Cbuild install` instead (or after).

You've got to do --recursive, btw, else you won't get the libraries
this program relies on. If you forget, just rm -rf the directory and
try again. Secret to life, really.
